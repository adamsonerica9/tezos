meta:
  id: ground__bool
  endian: be
enums:
  bool:
    0: false
    255: true
seq:
- id: bool
  type: u1
  enum: bool
