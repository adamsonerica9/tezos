(* Do not edit this file manually.
   This file was automatically generated from benchmark models
   If you wish to update a function in this file,
   a. update the corresponding model, or
   b. move the function to another module and edit it there. *)

[@@@warning "-33"]

module S = Saturation_repr
open S.Syntax

(* model sc_rollup/Sc_rollup_deserialize_output_proof_benchmark *)
(* fun size -> 7100. + (6. * size) *)
let cost_Sc_rollup_deserialize_output_proof_benchmark size =
  let size = S.safe_int size in
  let v0 = size in
  S.safe_int 7100 + (v0 * S.safe_int 6)

(* model sc_rollup/Sc_rollup_install_boot_sector_benchmark *)
(* fun size -> 13550. + (3.5 * size) *)
let cost_Sc_rollup_install_boot_sector_benchmark size =
  let size = S.safe_int size in
  let v0 = size in
  S.safe_int 13550 + ((v0 lsr 1) + (v0 * S.safe_int 3))

(* model sc_rollup/Sc_rollup_verify_output_proof_benchmark *)
(* fun size -> 103450. + (7. * size) *)
let cost_Sc_rollup_verify_output_proof_benchmark size =
  let size = S.safe_int size in
  let v0 = size in
  S.safe_int 103450 + (v0 * S.safe_int 7)
